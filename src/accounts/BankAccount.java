/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accounts;

/**
 *
 * @author S521743
 */
public class BankAccount {

    private double balance;

    public BankAccount() {
        setBalance(0);
    }

    public synchronized void deposit(double amount) {
        System.out.println("Depositing: " + amount);
        double newBalance = getBalance() + amount;
        System.out.println("New balance is " + newBalance);
        balance = newBalance;
        notifyAll();
    }

    public synchronized void withdraw(double amount) throws
            InterruptedException {
        while (balance < amount) {
            wait();
        }

        System.out.println("Withdrawing: " + amount);
        double newBalance = getBalance() - amount;
        System.out.println("New balance is " + newBalance);
        balance = newBalance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

}
